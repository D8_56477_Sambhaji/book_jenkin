CREATE TABLE book(
    book_id INTEGER PRIMARY KEY AUTO_INCREMENT, 
    book_title VARCHAR(50), 
    publisher_name VARCHAR(50), 
    author_name VARCHAR(50)
    );

    INSERT INTO book VALUES(1, 'Death', 'Washington', 'Sadhguru');