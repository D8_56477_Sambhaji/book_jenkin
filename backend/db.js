const mysql = require('mysql2')


const openConnection = () =>{

    const connection = mysql.createConnection({
        port: 3306,
        host: "demodb",
        user: "root",
        password: "root",
        database: "jan14"
    })

    connection.connect()

    return connection
}

module.exports = {
    openConnection
}